//
//  MeditoApp.swift
//  Medito
//
//  Created by Martin Lukacs on 25/04/2021.
//

import SwiftUI

@main
struct MeditoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
